//
//  OYYoutubeVideo.m
//
//  Created by Beniamin on 15.05.14.
//

#import "OYYoutubeVideo.h"

@implementation OYYoutubeVideo

- (NSString *)description
{
    return [NSString stringWithFormat:@"{uid:%@, thumbnail:%@, title: %@, info: %@, date: %@, viewCount: %@}", _uid, _thumbnail, _title, _info, _publishDate, _viewCount];
}

@end
