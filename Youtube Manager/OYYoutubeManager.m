//
//  OYYoutubeManager.m
//
//  Created by Beniamin Sarkisyan on 14.05.14.
//

#import "OYYoutubeManager.h"

#import "GTLYouTube.h"



//------Error-------//
static NSString *const kHasNotVideoErrorDomain  = @"HasNotVideoErrorDomain";

enum
{
    OYYoutubeRequestNotVideoOnChannel = 1001
};
//------Error-------//

static NSString *const kApiKey                  = @"AIzaSyCbbviuqo6zb7-jYRx97NvBCshJvq5pHzk";
static NSString *const kChannelID               = @"UCRKRGoo367_uweBlZ8PF4Nw";
static int const kYoutubeVideoLimit             = 25;

static GTLServiceYouTube *service;

typedef void(^ChannelQuery)(NSString *playlistID);
typedef void(^PlaylistItemsQuery)(NSString *videoIDs);
typedef void(^VideoQuery)(NSArray *videos);
typedef void(^QueryFailure)(NSError *error);

typedef void(^LoadVideoListSuccess)(NSArray *videoList);
typedef void(^LoadVideoListError)(NSString *title, NSString *message);

@interface OYYoutubeManager ()
{
    NSMutableArray *youtubeVideos;
    BOOL finishLoad;
}

@property (assign, nonatomic, readonly) ChannelQuery channelQuerySuccess;
@property (assign, nonatomic, readonly) PlaylistItemsQuery playlistItemsQuerySuccess;
@property (assign, nonatomic, readonly) VideoQuery videoQuerySuccess;
@property (assign, nonatomic, readonly) QueryFailure queryFailure;

@property (strong, nonatomic) LoadVideoListSuccess loadVideoListSuccess;
@property (strong, nonatomic) LoadVideoListError loadVideoListFailure;

@property (strong, nonatomic) NSString *nextPageToken;

- (void)executeChannelQueryWithSuccess:(void(^)(NSString *playlistID))success;

- (void)executePlaylistItemsQueryWithPlaylistID:(NSString *)playlistID
                                    withSuccess:(void(^)(NSString *videoIDs))success;

- (void)executeVideoListQueryWithVideoIDs:(NSString *)videoIDs
                              withSuccess:(void(^)(NSArray *videos))success;

@end

@implementation OYYoutubeManager

#pragma mark - Public methods -
#pragma mark Init methods
+ (id)youtubeManager
{
    return [[self alloc] init];
}

- (id)init
{    
    if ( self = [super init] )
    {
        service = [[GTLServiceYouTube alloc] init];
        service.APIKey = kApiKey;
        
        youtubeVideos = [@[] mutableCopy];

        finishLoad = NO;
    }
    
    return self;
}

#pragma mark Load methods
- (void)loadVideoListWithSuccess:(void(^)(NSArray *videoList))success
                     withFailure:(void(^)(NSString *title, NSString *message))failure
{
    if ( finishLoad )
        return;
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    [youtubeVideos removeAllObjects];

    self.loadVideoListSuccess = success;
    self.loadVideoListFailure = failure;
    
    [self executeChannelQueryWithSuccess:self.channelQuerySuccess];
}

#pragma mark Clear methods
- (void)clearCache
{
    [youtubeVideos removeAllObjects];
    self.nextPageToken = nil;
    finishLoad = NO;
}

#pragma mark - Private methods -
#pragma mark Queries methods
- (void)executeChannelQueryWithSuccess:(void(^)(NSString *playlistID))success
{
    GTLQueryYouTube *channelQuery = [GTLQueryYouTube queryForChannelsListWithPart:@"contentDetails"];
    channelQuery.identifier = kChannelID;
    
    [service executeQuery:channelQuery completionHandler:^(GTLServiceTicket *ticket, GTLYouTubeChannelListResponse *channelList, NSError *error) {
        if ( error )
        {
            self.queryFailure(error);
            return;
        }
        
        if ( !channelList.items.count )
        {
            self.queryFailure(nil);
            return;
        }
        
        GTLYouTubeChannel *channel = [channelList.items firstObject];
        NSString *playlistID = channel.contentDetails.relatedPlaylists.uploads;
        
        if ( !playlistID.length )
        {
            self.queryFailure(nil);
            return;
        }
        
        if ( success )
            success(playlistID);
    }];
}

- (void)executePlaylistItemsQueryWithPlaylistID:(NSString *)playlistID
                                    withSuccess:(void(^)(NSString *videoIDs))success
{
    GTLQueryYouTube *playlistItemsQuery = [GTLQueryYouTube queryForPlaylistItemsListWithPart:@"snippet, contentDetails"];
    playlistItemsQuery.playlistId = playlistID;
    playlistItemsQuery.maxResults = kYoutubeVideoLimit;
    
    if ( self.nextPageToken )
        playlistItemsQuery.pageToken = [self.nextPageToken copy];
    
    [service executeQuery:playlistItemsQuery completionHandler:^(GTLServiceTicket *ticket, GTLYouTubePlaylistItemListResponse *playlistItemList, NSError *error) {
        if ( error )
        {
            self.queryFailure(error);
            return;
        }
        
        if ( !playlistItemList.items.count )
        {
            self.queryFailure(nil);
            return;
        }
        
        finishLoad = ( !playlistItemList.nextPageToken && playlistItemList.prevPageToken );
        
        self.nextPageToken = playlistItemList.nextPageToken;
        
        NSMutableString *videoIDs = [@"" mutableCopy];
        
        NSArray *playlistItems = playlistItemList.items;
        
        for (GTLYouTubePlaylistItem *playlistItem in playlistItems)
        {
            GTLYouTubePlaylistItemSnippet *snippet = playlistItem.snippet;
            GTLYouTubePlaylistItemContentDetails *contentDetails = playlistItem.contentDetails;
            
            [videoIDs appendFormat:@"%@, ", contentDetails.videoId];
            
            OYYoutubeVideo *video = [OYYoutubeVideo new];
            video.uid = contentDetails.videoId;
            video.thumbnail = snippet.thumbnails.standard.url;
            video.title = snippet.title;
            video.info = snippet.descriptionProperty;
            video.publishDate = snippet.publishedAt.date;
            
            [youtubeVideos addObject:video];
        }
        
        if ( !youtubeVideos.count )
        {
            NSError *error = [self createHasNotVideoOnChannelError];
            self.queryFailure(error);
            
            return;
        }
        
        if ( success )
            success(videoIDs);
    }];
}

- (void)executeVideoListQueryWithVideoIDs:(NSString *)videoIDs
                              withSuccess:(void(^)(NSArray *videos))success
{
    GTLQueryYouTube *videosQuery = [GTLQueryYouTube queryForVideosListWithPart:@"statistics, player, contentDetails"];
    videosQuery.identifier = videoIDs;
    
    [service executeQuery:videosQuery completionHandler:^(GTLServiceTicket *ticket, GTLYouTubeVideoListResponse *videoList, NSError *error) {
        if ( error )
        {
            self.queryFailure(error);
            return;
        }
        
        if ( !videoList.items.count )
        {
            self.queryFailure(nil);
            return;
        }
        
        NSArray *videoListItems = videoList.items;
        
        for (GTLYouTubeVideo *video in videoListItems)
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.uid == %@", video.identifier];
            
            NSArray *filtredArray = [youtubeVideos filteredArrayUsingPredicate:predicate];
            
            if ( !filtredArray.count )
                continue;
            
            OYYoutubeVideo *_video = [filtredArray firstObject];
            _video.dislikeCount = video.statistics.dislikeCount;
            _video.favoriteCount = video.statistics.favoriteCount;
            _video.likeCount = video.statistics.likeCount;
            _video.viewCount = video.statistics.viewCount;
            _video.duration = video.contentDetails.duration;
            _video.embedHtml = video.player.embedHtml;
        }
        
        if ( success )
            success(youtubeVideos);
    }];
}

#pragma mark Queries blocks
- (ChannelQuery)channelQuerySuccess
{
    ChannelQuery block = ^(NSString *playlistID)
    {
        [self executePlaylistItemsQueryWithPlaylistID:playlistID
                                          withSuccess:self.playlistItemsQuerySuccess];
    };
    
    return block;
}

- (PlaylistItemsQuery)playlistItemsQuerySuccess
{
    PlaylistItemsQuery block = ^(NSString *videoIDs)
    {
        [self executeVideoListQueryWithVideoIDs:videoIDs
                                    withSuccess:self.videoQuerySuccess];
    };
    
    return block;
}

- (VideoQuery)videoQuerySuccess
{
    VideoQuery block = ^(NSArray *videos)
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if ( self.loadVideoListSuccess )
            self.loadVideoListSuccess(videos);
    };
    
    return block;
}

- (QueryFailure)queryFailure
{
    QueryFailure block = ^(NSError *error)
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        NSString *alertTitle = NSLocalizedString(@"Attention", nil);
        NSString *alertMessage = @"Что-то пошло не так";
        
        if ( error.code == NSURLErrorNotConnectedToInternet )
        {
            alertMessage = NSLocalizedString(@"Internet problem", nil);
        }
        else if ( error.code == OYYoutubeRequestNotVideoOnChannel )
        {
            alertMessage = NSLocalizedString(error.localizedFailureReason, nil);
        }
        
        if ( self.loadVideoListFailure )
            self.loadVideoListFailure(alertTitle, alertMessage);
    };
    
    return block;
}

#pragma mark Helpers
- (NSError *)createHasNotVideoOnChannelError
{
    NSString *reason = NSLocalizedString(@"Not video on the channel", nil);
    NSDictionary *userInfo = @{NSLocalizedFailureReasonErrorKey: reason};
    
    NSError *error = [NSError errorWithDomain:kHasNotVideoErrorDomain
                                         code:OYYoutubeRequestNotVideoOnChannel
                                     userInfo:userInfo];
    
    return error;
}

@end
