//
//  OYYoutubeManager.h
//
//  Created by Beniamin Sarkisyan on 14.05.14.
//

#import <Foundation/Foundation.h>

#import "OYYoutubeVideo.h"

///-----------------------------------------------------------
/// class OYYoutubeManager Description
///-----------------------------------------------------------
@interface OYYoutubeManager : NSObject

/**
 *  Init method for class
 *
 *  @return self
 */
+ (id)youtubeManager;

/**
 *  Load vide from youtube channel
 *
 *  @param success - block call when response success
 *  @param failure - block call when response failure
 */
- (void)loadVideoListWithSuccess:(void(^)(NSArray *videoList))success
                     withFailure:(void(^)(NSString *title, NSString *message))failure;

/**
 *  Clear temp properties
 */
- (void)clearCache;

@end