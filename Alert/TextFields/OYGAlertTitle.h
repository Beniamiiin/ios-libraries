//
//  OYGAlertTitle.h
//
//  Created by Beniamin Sarkisyan on 12.05.14.
//

#import "OYGTextField.h"

///-----------------------------------------------------------
/// class OYGAlertTitle Description
///-----------------------------------------------------------
@interface OYGAlertTitle : OYGTextField

/**
 *  Method of class for init alert title text field
 *
 *  @param message - alert title
 *
 *  @return OYGAlertTitle subclass of SPTextField
 */
+ (OYGAlertTitle *)alertTitle:(NSString *)title;

@end
