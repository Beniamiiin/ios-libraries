//
//  OYGAlertTitle.m
//
//  Created by Beniamin Sarkisyan on 12.05.14.
//

#import "OYGAlertTitle.h"

static int const kAlertTitleTextFieldHeight = 23;

static int const kAlertTitleTextFieldFontSize = 32;
static uint const kAlertTitleTextFieldTextColor = 0xb91503;
static NSString *const kAlertTitleTextFieldFontName = @"HelveticaNeue-CondensedBold";

@implementation OYGAlertTitle

+ (OYGAlertTitle *)alertTitle:(NSString *)title
{
    return [[self alloc] initWithText:title];
}

- (id)initWithText:(NSString *)text
{
    if ( self = [super initWithText:[text uppercaseString]] )
    {
        self.height = kAlertTitleTextFieldHeight;
        self.fontSize = kAlertTitleTextFieldFontSize;
        self.color = kAlertTitleTextFieldTextColor;
        self.fontName = kAlertTitleTextFieldFontName;
        self.hAlign = SPHAlignCenter;
        self.vAlign = SPVAlignCenter;
    }
    
    return self;
}

@end
