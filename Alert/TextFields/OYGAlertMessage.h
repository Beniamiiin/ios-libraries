//
//  OYGAlertMessage.h
//
//  Created by Beniamin Sarkisyan on 12.05.14.
//

#import "OYGTextField.h"

///-----------------------------------------------------------
/// class OYGAlertMessage Description
///-----------------------------------------------------------
@interface OYGAlertMessage : OYGTextField

/**
 *  Method of class for init alert message text field
 *
 *  @param message - alert message
 *
 *  @return OYGAlertMessage subclass of SPTextField
 */
+ (OYGAlertMessage *)alertMessage:(NSString *)message;

@end
