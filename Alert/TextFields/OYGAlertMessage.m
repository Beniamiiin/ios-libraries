//
//  OYGAlertMessage.m
//
//  Created by Beniamin Sarkisyan on 12.05.14.
//

#import "OYGAlertMessage.h"

static int const kAlertMessageTextFieldHeight = 15;

static int const kAlertMessageTextFieldFontSize = 14;
static uint const kAlertMessageTextFieldTextColor = 0x0563ec;
static NSString *const kAlertMessageTextFieldFontName = @"HelveticaNeue-Bold";

@implementation OYGAlertMessage

+ (OYGAlertMessage *)alertMessage:(NSString *)message
{
    return [[self alloc] initWithText:message];
}

- (id)initWithText:(NSString *)text
{
    if ( self = [super initWithText:text] )
    {
        self.height = kAlertMessageTextFieldHeight;
        self.fontSize = kAlertMessageTextFieldFontSize;
        self.color = kAlertMessageTextFieldTextColor;
        self.fontName = kAlertMessageTextFieldFontName;
        self.hAlign = SPHAlignCenter;
        self.vAlign = SPVAlignCenter;
    }
    
    return self;
}

@end
