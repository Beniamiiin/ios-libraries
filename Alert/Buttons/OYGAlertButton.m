//
//  OYGAlertButton.m
//
//  Created by Beniamin Sarkisyan on 12.05.14.
//

#import "OYGAlertButton.h"

static int const kAlertButtonTitleFontSize = 15;
static uint const kAlertButtonTitleTextColor = 0xfffeff;
static NSString *const kAlertButtonTitleFontName = @"HelveticaNeue-CondensedBold";

@implementation OYGAlertButton
{
    AlertButtonType _type;
}

+ (OYGAlertButton *)alertButtonWithType:(AlertButtonType)type withTitle:(NSString *)title
{
    return [[self alloc] initWithButtonType:type withTitle:title];
}

- (id)initWithButtonType:(AlertButtonType)type withTitle:(NSString *)title
{
    if ( self = [super init] )
    {
        NSString *buttonName;
        
        switch (type)
        {
            case AlertButtonTypeFirst:
                buttonName = @"alertFirstButton";
                break;
                
            case AlertButtonTypeSecond:
                buttonName = @"alertSecondButton";
                break;
        }
        
        OYGTexture *texture = [[OYGResourceManager sharedInstance] textureByName:buttonName];
        
        self = [OYGAlertButton buttonWithUpState:texture text:[title uppercaseString]];
        
        self.fontSize = kAlertButtonTitleFontSize;
        self.fontName = kAlertButtonTitleFontName;
        self.fontColor = kAlertButtonTitleTextColor;
        
        _type = type;
    }
    
    return self;
}

- (AlertButtonType)buttonType
{
    return _type;
}

- (void)setText:(NSString *)text
{
    [super setText:[text uppercaseString]];
}

@end
