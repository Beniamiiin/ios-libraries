//
//  OYGAlertButton.h
//
//  Created by Beniamin Sarkisyan on 12.05.14.
//

#import "OYGButton.h"

/**
 *  Alert button type enum
 */
typedef NS_ENUM(NSInteger, AlertButtonType)
{
    /**
     *  Left button type
     */
    AlertButtonTypeFirst,
    /**
     *  Right button type
     */
    AlertButtonTypeSecond
};

///-----------------------------------------------------------
/// class OYGAlertButton Description
///-----------------------------------------------------------
@interface OYGAlertButton : OYGButton

/**
 *  Method of class for init alert button
 *
 *  @param type  - button type
 *  @param title - button title
 *
 *  @return OYGAlertButton
 */
+ (OYGAlertButton *)alertButtonWithType:(AlertButtonType)type withTitle:(NSString *)title;

/**
 *  Method of object for init alert button
 *
 *  @param type  - button type
 *  @param title - button title
 *
 *  @return OYGAlertButton
 */
- (id)initWithButtonType:(AlertButtonType)type withTitle:(NSString *)title;

/**
 *  Return button type
 *
 *  @return AlertButtonType
 */
- (AlertButtonType)buttonType;

@end
