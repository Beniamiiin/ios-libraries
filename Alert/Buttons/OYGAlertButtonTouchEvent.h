//
//  OYGAlertButtonTouchEvent.h
//
//  Created by Beniamin Sarkisyan on 12.05.14.
//

#import "SPEvent.h"

#import "OYGAlertButton.h"

///-----------------------------------------------------------
/// class OYGAlertButtonTouchEvent Description
///-----------------------------------------------------------
@interface OYGAlertButtonTouchEvent : SPEvent

/**
 *  Touched button type
 */
@property (assign, nonatomic) AlertButtonType buttonType;

@end
