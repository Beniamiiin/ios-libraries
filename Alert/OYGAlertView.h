//
//  OYGAlertView.h
//
//  Created by Beniamin Sarkisyan on 12.05.14.
//

#import "OYGSprite.h"

#import "OYGAlertContainer.h"

@class OYGAlertView;

typedef NS_ENUM(NSInteger, AlertPriority)
{
    AlertPriorityNormal,
    AlertPriorityHight
};

extern NSString *const kAlertSpriteName;

@protocol OYGAlertViewDelegate <NSObject>

@optional
/**
 *  Call when alert button touch
 *
 *  @param alertView  - self
 *  @param buttonType - button touch type
 */
- (void)alertView:(OYGAlertView *)alertView clickedButtonType:(AlertButtonType)buttonType;

/**
 *  Call when alert will show
 *
 *  @param alertView - self
 */
- (void)alertViewWillShow:(OYGAlertView *)alertView;

/**
 *  Call when alert did show
 *
 *  @param alertView - self
 */
- (void)alertViewDidShow:(OYGAlertView *)alertView;

/**
 *  Call when alert will hide
 *
 *  @param alertView - self
 */
- (void)alertViewWillHide:(OYGAlertView *)alertView;

/**
 *  Call when alert did hide
 *
 *  @param alertView - self
 */
- (void)alertViewDidHide:(OYGAlertView *)alertView;

@end

///-----------------------------------------------------------
/// class OYGAlertView Description
///-----------------------------------------------------------
@interface OYGAlertView : OYGSprite

@property (weak, nonatomic) id<OYGAlertViewDelegate> delegate;

/**
 *  Alert message
 */
@property (strong, nonatomic) NSString *message;

/**
 *  Alert title
 */
@property (strong, nonatomic) NSString *title;

/**
 *  Alert priority
 *  @see AlertPriority
 */
@property (assign, nonatomic) AlertPriority alertPriority;

/**
 *  Method of class for init alert view
 *
 *  @param title                        - alert title
 *  @param message                      - alert message
 *  @param delegate                     - alert delegate
 *  @param firstButtonTitle             - alert first button title
 *  @param secondButtonTitle            - alert second button title
 *  @param firstButtonCompletionBlock   - left button completion block
 *  @param secondButtonCompletionBlock  - right button completion block
 *
 *  @return OYGAlertView
 */
+ (OYGAlertView *)alertViewWithTitle:(NSString *)title
                             message:(NSString *)message
                            delegate:(id<OYGAlertViewDelegate>)delegate
                    firstButtonTitle:(NSString *)firstButtonTitle
                   secondButtonTitle:(NSString *)secondButtonTitle
          completionFirstButtonBlock:(void(^)())firstButtonCompletionBlock
         completionSecondButtonBlock:(void(^)())secondButtonCompletionBlock;

/**
 *  Method of object for init alert view
 *
 *  @param title                        - alert title
 *  @param message                      - alert message
 *  @param delegate                     - alert delegate
 *  @param firstButtonTitle             - alert first button title
 *  @param secondButtonTitle            - alert second button title
 *  @param firstButtonCompletionBlock   - left button completion block
 *  @param secondButtonCompletionBlock  - right button completion block
 *
 *  @return OYGAlertView
 */
- (id)initWithTitle:(NSString *)title
            message:(NSString *)message
           delegate:(id<OYGAlertViewDelegate>)delegate
   firstButtonTitle:(NSString *)firstButtonTitle
  secondButtonTitle:(NSString *)secondButtonTitle
completionFirstButtonBlock:(void(^)())firstButtonCompletionBlock
completionSecondButtonBlock:(void(^)())secondButtonCompletionBlock;

/**
 *  Return alert view container
 *
 *  @return OYGAlertContainer
 */
- (OYGAlertContainer *)alertContainer;

/**
 *  Show alert
 */
- (void)show;

/**
 *  Hide alert
 */
- (void)hide;

@end
