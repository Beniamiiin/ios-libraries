//
//  OYGAlertContainer.h
//
//  Created by Beniamin Sarkisyan on 12.05.14.
//

#import "OYGSprite.h"

#import "OYGAlertButton.h"

extern NSString *const kAlertButtonTouchEvent;

///-----------------------------------------------------------
/// class OYGAlertContainer Description
///-----------------------------------------------------------
@interface OYGAlertContainer : OYGSprite

/**
 *  Method of class for init alert container
 *
 *  @param title             - alert title
 *  @param message           - alert message
 *  @param firstButtonTitle  - alert first button title
 *  @param secondButtonTitle - alert second button title
 *
 *  @return OYGAlertContainer
 */
+ (OYGAlertContainer *)alertContainerWithTitle:(NSString *)title
                                       message:(NSString *)message
                              firstButtonTitle:(NSString *)firstButtonTitle
                             secondButtonTitle:(NSString *)secondButtonTitle;

/**
 *  Method of object for init alert container
 *
 *  @param title             - alert title
 *  @param message           - alert message
 *  @param firstButtonTitle  - alert first button title
 *  @param secondButtonTitle - alert second button title
 *
 *  @return OYGAlertContainer
 */
- (id)initWithTitle:(NSString *)title
            message:(NSString *)message
   firstButtonTitle:(NSString *)firstButtonTitle
  secondButtonTitle:(NSString *)secondButtonTitle;

@end
