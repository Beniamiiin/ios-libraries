//
//  OYGAlertContainer.m
//
//  Created by Beniamin Sarkisyan on 12.05.14.
//

#import "OYGAlertContainer.h"

#import "OYGImage.h"

#import "OYGAlertTitle.h"
#import "OYGAlertMessage.h"

#import "OYGAlertButtonTouchEvent.h"
#import "SPTextField+TextHeight.h"

NSString *const kAlertButtonTouchEvent = @"OYGAlertButtonTouchEventID";

@implementation OYGAlertContainer
{
    OYGImage *bg;
    
    OYGAlertTitle *alertTitle;
    OYGAlertMessage *alertMessage;
    
    OYGAlertButton *firstButton;
    OYGAlertButton *secondButton;
}

+ (OYGAlertContainer *)alertContainerWithTitle:(NSString *)title
                                       message:(NSString *)message
                              firstButtonTitle:(NSString *)firstButtonTitle
                             secondButtonTitle:(NSString *)secondButtonTitle
{
    return [[self alloc] initWithTitle:title
                               message:message
                      firstButtonTitle:firstButtonTitle
                     secondButtonTitle:secondButtonTitle];
}

- (id)initWithTitle:(NSString *)title
            message:(NSString *)message
   firstButtonTitle:(NSString *)firstButtonTitle
  secondButtonTitle:(NSString *)secondButtonTitle
{
    if ( self = [super init] )
    {
        // setup views
        [self setupBG];
        [self setupTitle:title];
        [self setupMessage:message];
        
        // add to container
        [self addChild:bg];
        [self addChild:alertTitle];
        [self addChild:alertMessage];
        
        // setup buttons
        [self setupButtonsWithFirstButtonTitle:firstButtonTitle
                             secondButtonTitle:secondButtonTitle];
    }
    
    return self;
}

#pragma mark - Setup Methods
- (void)setupBG
{
    OYGTexture *texture = [[OYGResourceManager sharedInstance] textureByName:@"alertBG"];
    bg = [OYGImage imageWithTexture:texture];
}

- (void)setupTitle:(NSString *)title
{
    alertTitle = [OYGAlertTitle alertTitle:title];
    alertTitle.x = 10;
    alertTitle.y = 36;
    alertTitle.width = self.width - alertTitle.x*2;
}

- (void)setupMessage:(NSString *)message
{
    alertMessage = [OYGAlertMessage alertMessage:message];
    alertMessage.x = 20;
    alertMessage.y = alertTitle.y + alertTitle.height + 20;
    alertMessage.width = self.width - alertMessage.x*2;
    alertMessage.height = [alertMessage textHeight] + 10;
}

- (void)setupButtonsWithFirstButtonTitle:(NSString *)firstButtonTitle
                       secondButtonTitle:(NSString *)secondButtonTitle
{
    firstButton = [OYGAlertButton alertButtonWithType:AlertButtonTypeFirst withTitle:NSLocalizedStringFromTable(@"OK", @"Garden", nil)];
    firstButton.x = self.width/2 - firstButton.width/2;
    firstButton.y = alertMessage.y + alertMessage.height + 15;
    
    bg.height = firstButton.y + firstButton.height + 20;
    
    __weak typeof(self) weakSelf = self;
    __weak typeof(firstButton) weakFirstButton = firstButton;
    [firstButton addEventListenerForType:SP_EVENT_TYPE_TRIGGERED block:^(id event) {
        OYGAlertButtonTouchEvent *buttonEvent = [OYGAlertButtonTouchEvent eventWithType:kAlertButtonTouchEvent bubbles:YES];
        buttonEvent.buttonType = [weakFirstButton buttonType];
        
        [weakSelf dispatchEvent:buttonEvent];
    }];
    
    if ( !firstButtonTitle || !secondButtonTitle )
    {
        [self addChild:firstButton];
        return;
    }
    
    // setup first button
    if ( firstButtonTitle )
    {
        firstButton.text = firstButtonTitle;
        firstButton.x = 19;
        [self addChild:firstButton];
    }
    
    // setup second button
    if ( secondButtonTitle )
    {
        secondButton = [OYGAlertButton alertButtonWithType:AlertButtonTypeSecond withTitle:secondButtonTitle];
        secondButton.x = firstButton.x + firstButton.width + 12;
        secondButton.y = firstButton.y;
        
        __weak typeof(secondButton) weakSecondButton = secondButton;
        
        [secondButton addEventListenerForType:SP_EVENT_TYPE_TRIGGERED block:^(id event) {
            OYGAlertButtonTouchEvent *buttonEvent = [OYGAlertButtonTouchEvent eventWithType:kAlertButtonTouchEvent bubbles:YES];
            buttonEvent.buttonType = [weakSecondButton buttonType];
            
            [weakSelf dispatchEvent:buttonEvent];
        }];
        
        [self addChild:secondButton];
    }
}

#pragma mark - Helpers
- (float)width
{
    return bg.width;
}

- (float)height
{
    return bg.height;
}

- (void)setTouchable:(BOOL)touchable
{
    bg.touchable = NO;
    alertTitle.touchable = NO;
    alertMessage.touchable = NO;
    firstButton.touchable = NO;
    secondButton.touchable = NO;
    
    [super setTouchable:touchable];
}

@end
