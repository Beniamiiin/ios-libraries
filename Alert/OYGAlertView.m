//
//  OYGAlertView.m
//
//  Created by Beniamin Sarkisyan on 12.05.14.
//

#import "OYGAlertView.h"

#import "OYGSharePopup.h"

#import "OYGButton.h"
#import "OYGAlertTitle.h"
#import "OYGAlertMessage.h"

#import "OYGAlertButtonTouchEvent.h"

static uint const kBackgrounColor = 0x0;
static float const kBackgroundAlpha = 0.7;

NSString *const kAlertSpriteName = @"kAlertSpriteName";

typedef void(^CompletionBlock)();

@implementation OYGAlertView
{
    struct {
        unsigned int didTouch       : 1;
        unsigned int didWillShow    : 1;
        unsigned int didDidShow     : 1;
        unsigned int didWillHide    : 1;
        unsigned int didDidHide     : 1;
    } _delegateFlags;
    
    SPQuad *bg;
    
    OYGAlertContainer *alertContainer;
    
    CompletionBlock _firstCompletionBlock;
    CompletionBlock _secondCompletionBlock;
}

+ (OYGAlertView *)alertViewWithTitle:(NSString *)title
                             message:(NSString *)message
                            delegate:(id<OYGAlertViewDelegate>)delegate
                    firstButtonTitle:(NSString *)firstButtonTitle
                   secondButtonTitle:(NSString *)secondButtonTitle
          completionFirstButtonBlock:(void(^)())firstButtonCompletionBlock
         completionSecondButtonBlock:(void(^)())secondButtonCompletionBlock
{
    return [[self alloc] initWithTitle:title
                               message:message
                              delegate:delegate
                      firstButtonTitle:firstButtonTitle
                     secondButtonTitle:secondButtonTitle
            completionFirstButtonBlock:firstButtonCompletionBlock
           completionSecondButtonBlock:secondButtonCompletionBlock];
}

- (id)initWithTitle:(NSString *)title
            message:(NSString *)message
           delegate:(id<OYGAlertViewDelegate>)delegate
   firstButtonTitle:(NSString *)firstButtonTitle
  secondButtonTitle:(NSString *)secondButtonTitle
completionFirstButtonBlock:(void(^)())firstButtonCompletionBlock
completionSecondButtonBlock:(void(^)())secondButtonCompletionBlock
{
    if ( self = [super init] )
    {
        self.name = kAlertSpriteName;
        self.alertPriority = AlertPriorityNormal;
        
        OYGAlertView *existingAlert = (OYGAlertView*)[Sparrow.stage childByName:self.name];
        
        if ( existingAlert )
        {
            if ( existingAlert.alertPriority > self.alertPriority )
                return nil;
        }
        
        // setter
        self.message = message;
        self.title = title;
        self.delegate = delegate;
        
        // completion blocks
        _firstCompletionBlock = firstButtonCompletionBlock;
        _secondCompletionBlock = secondButtonCompletionBlock;
        
        // setup views
        [self setupBG];
        [self setupContainerWithTitle:title
                              message:message
                     firstButtonTitle:firstButtonTitle
                    secondButtonTitle:secondButtonTitle];
        
        // add to scene
        [self addChild:bg];
        [self addChild:alertContainer];
        
        // add touch event
        [self addEventListener:@selector(alertButtonTouch:) atObject:self forType:kAlertButtonTouchEvent];
    }
    
    return self;
}

- (void)setDelegate:(id<OYGAlertViewDelegate>)delegate
{
    _delegate = delegate;
    
    if ( _delegate )
    {
        _delegateFlags.didTouch = [_delegate respondsToSelector:@selector(alertView:clickedButtonType:)];
        _delegateFlags.didWillShow = [_delegate respondsToSelector:@selector(alertViewWillShow:)];
        _delegateFlags.didDidShow = [_delegate respondsToSelector:@selector(alertViewDidShow:)];
        _delegateFlags.didWillHide = [_delegate respondsToSelector:@selector(alertViewWillHide:)];
        _delegateFlags.didDidHide = [_delegate respondsToSelector:@selector(alertViewDidHide:)];
    }
    else
    {
        _delegateFlags.didTouch = NO;
        _delegateFlags.didWillShow = NO;
        _delegateFlags.didDidShow = NO;
        _delegateFlags.didWillHide = NO;
        _delegateFlags.didDidHide = NO;
    }
}

#pragma mark - Setup Methods
- (void)setupBG
{
    bg = [SPQuad quadWithWidth:Sparrow.stage.width height:Sparrow.stage.height color:kBackgrounColor];
    bg.alpha = kBackgroundAlpha;
}

- (void)setupContainerWithTitle:(NSString *)title
                        message:(NSString *)message
               firstButtonTitle:(NSString *)firstButtonTitle
              secondButtonTitle:(NSString *)secondButtonTitle
{
    alertContainer = [OYGAlertContainer alertContainerWithTitle:title
                                                        message:message
                                               firstButtonTitle:firstButtonTitle
                                              secondButtonTitle:secondButtonTitle];
    
    alertContainer.x = self.width/2 - alertContainer.width/2;
    alertContainer.y = self.height/2 - alertContainer.height/2;
}

#pragma mark - Touch Button Methods
- (void)alertButtonTouch:(OYGAlertButtonTouchEvent *)event
{
    [self hide];
    
    [[OYGSoundManager sharedInstance] playSoundWithName:@"buttonClickSound"];
    
    if ( event.buttonType == AlertButtonTypeFirst && _firstCompletionBlock )
    {
        _firstCompletionBlock();
    }
    else if ( event.buttonType == AlertButtonTypeSecond && _secondCompletionBlock )
    {
        _secondCompletionBlock();
    }
    else if ( _delegateFlags.didTouch )
    {
        [_delegate alertView:self clickedButtonType:event.buttonType];
    }
}

#pragma mark - Helpers
- (OYGAlertContainer *)alertContainer
{
    return alertContainer;
}

- (float)width
{
    return bg.width;
}

- (float)height
{
    return bg.height;
}

#pragma mark - Show/Hide Methods
- (void)show
{
    if ( self.alertPriority != AlertPriorityHight )
    {
        if ( [Sparrow.stage childByName:kPopupSpriteName] )
            return;
    }
    
    if ( [Sparrow.stage childByName:self.name] )
        return;
        
    self.alpha = 0;
    
    [Sparrow.stage addChild:self];
    
    SPTween *tween = [SPTween tweenWithTarget:self time:0.3];
    [tween fadeTo:1];
    
    __weak typeof(self) weakSelf = self;
    
    tween.onStart = ^{
        if ( _delegateFlags.didWillShow )
        {
            [_delegate alertViewWillShow:weakSelf];
        }
        
        [self changeAlphaForTableView:0 duration:0.2];
    };
    
    tween.onComplete = ^{
        if ( _delegateFlags.didDidShow )
        {
            [_delegate alertViewDidShow:weakSelf];
        }
    };
    
    [Sparrow.juggler addObject:tween];
}

- (void)hide
{
    SPTween *tween = [SPTween tweenWithTarget:self time:0.3];
    [tween fadeTo:0];
    
    __weak typeof(self) weakSelf = self;

    tween.onStart = ^{
        if ( _delegateFlags.didWillHide )
        {
            [_delegate alertViewWillHide:weakSelf];
        }
        
        [self changeAlphaForTableView:1 duration:0.2];
    };
    
    tween.onComplete = ^{
        [alertContainer removeAllChildren];
        [self removeAllChildren];
        [self removeFromParent];
        
        if ( _delegateFlags.didDidHide )
        {
            [_delegate alertViewDidHide:weakSelf];
        }
    };
    
    [Sparrow.juggler addObject:tween];
}

- (void)changeAlphaForTableView:(float)alpha duration:(float)duration
{
    NSArray *subviews = Sparrow.currentController.view.subviews;
    
    if ( subviews.count )
    {
        for (UIView *view in subviews)
        {
            if ( [view isKindOfClass:[UITableView class]] )
            {
                UITableView *tableView = [subviews firstObject];
                
                [UIView animateWithDuration:duration animations:^{
                    tableView.alpha = alpha;
                }];
                
                return;
            }
        }
    }
}

@end
