//
//  OYSocialManager.m
//
//  Created by Beniamin on 09.06.14.
//

#import "OYSocialManager.h"

@interface OYSocialManager ()
{
    OYVKSocial *vkSocial;
    OYFBSocial *fbSocial;
    OYTwitterSocial *twitterSocial;
}

@end

static OYSocialManager *sharedSocialManagerInstance;

@implementation OYSocialManager

+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        sharedSocialManagerInstance = [[super alloc] initInstance];
    });
    
    return sharedSocialManagerInstance;
}

- (instancetype) initInstance
{
    if ( self = [super init] )
    {
        vkSocial = [OYVKSocial setup];
        fbSocial = [OYFBSocial setup];
        twitterSocial = [OYTwitterSocial setup];
    }
    
    return self;
}

#pragma mark - Authrization status
- (BOOL)vkIsSimpleAuthorized
{
    return vkSocial.isSimpleAuthorized;
}

- (BOOL)vkIsAdvancedAuthorized
{
    return vkSocial.isAdvancedAuthorized;
}

- (BOOL)facebookIsSimpleAuthorized
{
    return fbSocial.isSimpleAuthorized;
}

- (BOOL)facebookIsAdvancedAuthorized
{
    return fbSocial.isAdvancedAuthorized;
}

- (BOOL)twitterIsAuthorized
{
    return twitterSocial.isSimpleAuthorized;
}

#pragma mark - Authrization methods
- (void)loginToVKWithSuccess:(void (^)())success
                     failure:(void (^)(NSError *error))failure
{
    [vkSocial authorizeWithSuccess:success
                           failure:failure];
}

- (void)loginToFacebookWithSuccess:(void (^)())success
                           failure:(void (^)(NSError *error))failure
{
    [fbSocial authorizeWithSuccess:success
                           failure:failure];
}

- (void)loginToTwitterWithSuccess:(void (^)())success
                          failure:(void (^)(NSError *error))failure
{
    [twitterSocial authorizeWithSuccess:success
                                failure:failure];
}

#pragma mark - Post to wall
- (void)postToVKWall:(id)message
             success:(void(^)())success
             failure:(void(^)(NSError *error))failure
{
    [vkSocial postToWall:message
                 success:success
                 failure:failure];
}

- (void)postToFacebookWall:(id)message
                   success:(void(^)())success
                   failure:(void(^)(NSError *error))failure
{
    [fbSocial postToWall:message
                 success:success
                 failure:failure];
}

- (void)postToTwitterWall:(id)message
                  success:(void(^)())success
                  failure:(void(^)(NSError *error))failure
{
    [twitterSocial postToWall:message
                      success:success
                      failure:failure];
}

@end
