//
//  OYSocialManagerHelper.h
//
//  Created by Beniamin on 09.06.14.
//

#import <Foundation/Foundation.h>

extern NSString *const OYSocialErrorDomain;

enum
{
    /**
     *  Indicates problem with internet connection
     */
    OYLostInternetConnection = -1001
};

typedef NS_ENUM(NSInteger, OYLoaderType)
{
    OYLoaderTypeDefault,
    OYLoaderTypeCustom
};

@interface OYSocialManagerHelper : NSObject

+ (void)showLoader;

+ (void)hideLoader;

+ (NSError *)createErrorWithTitle:(NSString *)title
                          message:(NSString *)message
                             code:(NSInteger)code;

+ (BOOL)hasInternet:(NSError **)error;

+ (void)setLoaderContainer:(UIViewController *)container;
+ (UIViewController*)loginContainer;

+ (void)setLoaderType:(OYLoaderType)type;

@end
