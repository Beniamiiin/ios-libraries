//
//  OYSocialManager.h
//
//  Created by Beniamin on 09.06.14.
//

#import <Foundation/Foundation.h>

#import "OYSocial.h"
#import "OYVKSocial.h"
#import "OYFBSocial.h"
#import "OYTwitterSocial.h"

@interface OYSocialManager : NSObject

+ (instancetype)sharedInstance;

+ (instancetype) alloc __attribute__((unavailable("alloc not available, call sharedInstance instead")));
- (instancetype) init __attribute__((unavailable("init not available, call sharedInstance instead")));
+ (instancetype) new __attribute__((unavailable("new not available, call sharedInstance instead")));

@property (assign, nonatomic, readonly) BOOL vkIsSimpleAuthorized;
@property (assign, nonatomic, readonly) BOOL vkIsAdvancedAuthorized;

@property (assign, nonatomic, readonly) BOOL facebookIsSimpleAuthorized;
@property (assign, nonatomic, readonly) BOOL facebookIsAdvancedAuthorized;

@property (assign, nonatomic, readonly) BOOL twitterIsAuthorized;

- (void)loginToVKWithSuccess:(void (^)())success
                     failure:(void (^)(NSError *error))failure;

- (void)loginToFacebookWithSuccess:(void (^)())success
                           failure:(void (^)(NSError *error))failure;

- (void)loginToTwitterWithSuccess:(void (^)())success
                          failure:(void (^)(NSError *error))failure;

- (void)postToVKWall:(id)message
             success:(void(^)())success
             failure:(void(^)(NSError *error))failure;

- (void)postToFacebookWall:(id)message
                   success:(void(^)())success
                   failure:(void(^)(NSError *error))failure;

- (void)postToTwitterWall:(id)message
                  success:(void(^)())success
                  failure:(void(^)(NSError *error))failure;

@end