//
//  OYVKSocial.m
//
//  Created by Beniamin on 09.06.14.
//

#import "OYVKSocial.h"

#import "OYSocialManagerHelper.h"

#import <VK-ios-sdk/VKSdk.h>

static NSString *const VKPlistKey                    = @"VK";
static NSString *const VKPlistAppIDKey               = @"VKAppID";
static NSString *const VKPlistSimplePermissionsKey   = @"VKSimplePermissions";
static NSString *const VKPlistAdvancedPermissionsKey = @"VKAdvancedPermissions";
static NSString *const VKUserDefaultsLoginTypeKey    = @"VKUserDefaultsLoginTypeKey";

typedef NS_ENUM(NSInteger, VKLoginType)
{
    VKLoginTypeUnknown = -1,
    VKLoginWithSimplePermissions,
    VKLoginWithAdvancedPermissions
};

@interface OYVKSocial () <VKSdkDelegate>
{
    RequestSuccess vkRequestSuccess;
    RequestFailure vkRequestFailure;
    
    NSString *VKAppID;
    
    NSArray *VKSimplePermissions;
    NSArray *VKAdvancedPermissions;
    
    VKLoginType vkTempLoginType;
    
    VKApiWall *VKWall;
}

@property (assign, nonatomic, readwrite) VKLoginType vkLoginType;

- (void)p_loginToVKWithPermissions:(NSArray *)permissions
                         loginType:(int)loginType
                           success:(void (^)())success
                           failure:(void (^)(NSError *error))failure;

- (void)p_postToVKWall:(id)message
               success:(void (^)())success
               failure:(void (^)(NSError *error))failure;

- (void)p_postToVKWall:(id)message;

@end

@implementation OYVKSocial

#pragma mark - Public methods -

#pragma mark Initialize
+ (OYVKSocial *)setup
{
    return [[self alloc] init];
}

- (id)init
{
    if ( self = [super init] )
    {
        NSDictionary *infoPlistSocialSettings = [[NSBundle mainBundle] objectForInfoDictionaryKey:SocialSettingsPlistKey];
        
        // Setup permissions
        VKSimplePermissions = [infoPlistSocialSettings[VKPlistKey][VKPlistSimplePermissionsKey] copy];
        VKAdvancedPermissions = [infoPlistSocialSettings[VKPlistKey][VKPlistAdvancedPermissionsKey] copy];
        
        // Setup app ID and SecretKey
        VKAppID = [infoPlistSocialSettings[VKPlistKey][VKPlistAppIDKey] copy];
        
        // Init
        [VKSdk initializeWithDelegate:self andAppId:VKAppID];
        
        VKWall = [VKApi wall];
    }
    
    return self;
}

#pragma mark Authorization methods
- (void)authorizeWithSuccess:(void (^)())success
                     failure:(void (^)(NSError *error))failure
{
    [self p_loginToVKWithPermissions:VKAdvancedPermissions
                           loginType:VKLoginWithAdvancedPermissions
                             success:success
                             failure:failure];
}

#pragma mark Post to wall
- (void)postToWall:(id)message
           success:(void(^)())success
           failure:(void(^)(NSError *error))failure
{
    [self p_postToVKWall:[message copy]
                 success:success
                 failure:failure];
}

#pragma mark Getters
- (BOOL)isSimpleAuthorized
{
    return [VKSdk wakeUpSession];
}

- (BOOL)isAdvancedAuthorized
{
    return self.vkLoginType == VKLoginWithAdvancedPermissions;
}

#pragma mark - Private methods -

#pragma mark Authorization methods
- (void)p_loginToVKWithSimplePermissionsWithSuccess:(void (^)())success
                                            failure:(void (^)(NSError *error))failure
{
    [self p_loginToVKWithPermissions:VKSimplePermissions
                           loginType:VKLoginWithSimplePermissions
                             success:success
                             failure:failure];
}

- (void)p_loginToVKWithPermissions:(NSArray *)permissions
                         loginType:(int)loginType
                           success:(void (^)())success
                           failure:(void (^)(NSError *error))failure
{
    NSError *error;
    BOOL internet = [OYSocialManagerHelper hasInternet:&error];
    
    if ( !internet )
    {
        if ( failure && error )
            failure(error);
        
        return;
    }
    
    vkRequestSuccess = success;
    vkRequestFailure = failure;
    vkTempLoginType = loginType;
    
    if ( [VKSdk wakeUpSession] && self.vkLoginType == VKLoginWithAdvancedPermissions )
        return;
    
    [VKSdk authorize:permissions
        revokeAccess:YES
          forceOAuth:YES
               inApp:NO
             display:VK_DISPLAY_IOS];
}

#pragma mark Post to wall
- (void)p_postToVKWall:(id)message
               success:(void (^)())success
               failure:(void (^)(NSError *error))failure
{
    NSError *error;
    BOOL internet = [OYSocialManagerHelper hasInternet:&error];
    
    if ( !internet )
    {
        if ( failure && error )
            failure(error);
        
        return;
    }
    
    if ( self.simpleAuthorized )
    {
        vkRequestSuccess = success;
        vkRequestFailure = failure;
        
        [self p_postToVKWall:message];
        return;
    }
    
    void(^_success)() = ^{
        [self p_postToVKWall:message
                     success:success
                     failure:failure];
    };
    
    void(^_failure)(NSError *) = ^(NSError *error)
    {
        if ( !failure )
            return;
        
        NSError *_error = [self p_handleVKPostError:error];
        
        if ( _error )
            failure(_error);
        else
            failure(error);
    };
    
    [self p_loginToVKWithSimplePermissionsWithSuccess:_success
                                            failure:_failure];
}

- (void)p_postToVKWall:(id)message
{
    [OYSocialManagerHelper showLoader];
    
    VKRequest *request;
    
    if ( [message isKindOfClass:[NSDictionary class]] )
        request = [VKWall post:message];
    else
        request = [VKWall post:@{@"message": message}];

    request.attempts = 5;
    
    void(^success)(VKResponse *) = ^(VKResponse *response)
    {
        [OYSocialManagerHelper hideLoader];
        
        if ( vkRequestSuccess )
            vkRequestSuccess();
    };
    
    void(^failure)(NSError *) = ^(NSError *error)
    {
        [OYSocialManagerHelper hideLoader];
        
        NSError *_error = [self p_handleVKPostError:error];
        
        if ( vkRequestFailure && _error )
            vkRequestFailure(_error);
    };
    
    [request executeWithResultBlock:success
                         errorBlock:failure];
}

#pragma mark Helpers
- (NSError *)p_handleVKPostError:(NSError *)error
{
    NSString *description = @"Some went wrong";
    NSString *reason = error.localizedDescription;
    NSInteger code = error.code;
    
    if ( error.code == VK_API_ERROR )
    {
        VKError *vkError = error.vkError;
        
        if ( [vkError.errorMessage isEqualToString:@"User authorization failed: user revoke access for this token."] ||
            [vkError.errorMessage isEqualToString:@"Permission to perform this action is denied"] )
        {
            reason = @"You have denied the application to publish messages on the wall.";
            code = OYNotShowError;
        }
        else if ( [vkError.errorMessage isEqualToString:@"Internal server error: could not get application"] )
        {
            reason = @"Try again later";
            code = OYVKUnkwonError;
        }
    }
    else
    {
        if ( error.code == OYLostInternetConnection )
        {
            reason = @"Problem with internet connection";
            code = OYLostInternetConnection;
        }
        else if ( error.code == 1010 )
        {
            code = OYNotShowError;
        }
    }
    
    NSError *_error = [OYSocialManagerHelper createErrorWithTitle:description
                                                          message:reason
                                                             code:code];
    
    return _error;
}

#pragma mark VKSDKDelegate
- (void)vkSdkReceivedNewToken:(VKAccessToken*)newToken
{
    if ( self.vkLoginType != VKLoginWithAdvancedPermissions )
        self.vkLoginType = vkTempLoginType;
    
    vkTempLoginType = VKLoginTypeUnknown;
    
    if ( vkRequestSuccess )
        vkRequestSuccess();
}

- (void)vkSdkAcceptedUserToken:(VKAccessToken *)token
{
    if ( self.vkLoginType != VKLoginWithAdvancedPermissions )
        self.vkLoginType = vkTempLoginType;
    
    vkTempLoginType = VKLoginTypeUnknown;
    
    if ( vkRequestSuccess )
        vkRequestSuccess();
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller
{
    [[OYSocialManagerHelper loginContainer] presentViewController:controller animated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    }];
}

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError
{
    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    [vc presentIn:[OYSocialManagerHelper loginContainer]];
}

- (void)vkSdkUserDeniedAccess:(VKError*)authorizationError
{
    vkTempLoginType = VKLoginTypeUnknown;
    
    if ( authorizationError.errorCode == VK_API_ERROR )
    {
        NSError *error = [OYSocialManagerHelper createErrorWithTitle:@"Something went wrong"
                                                             message:authorizationError.errorMessage
                                                                code:OYVKLoginErrorUserDeniedRequest];
        
        if ( vkRequestFailure )
            vkRequestFailure(error);
        
        return;
    }
}

- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken
{
    vkTempLoginType = VKLoginTypeUnknown;
    
    if ( vkRequestFailure )
        vkRequestFailure(nil);
}

- (void)vkSdkRenewedToken:(VKAccessToken *)newToken
{
    if ( self.vkLoginType != VKLoginWithAdvancedPermissions )
        self.vkLoginType = vkTempLoginType;
    
    vkTempLoginType = VKLoginTypeUnknown;
    
    if ( vkRequestSuccess )
        vkRequestSuccess();
}

#pragma mark User defaults
- (VKLoginType)vkLoginType
{
    NSNumber *vkLoginType = [[NSUserDefaults standardUserDefaults] objectForKey:VKUserDefaultsLoginTypeKey];
    
    if ( !vkLoginType )
        return VKLoginTypeUnknown;
    
    return vkLoginType.longValue;
}

- (void)setVkLoginType:(VKLoginType)newVkLoginType
{
    NSNumber *vkLoginType = [[NSUserDefaults standardUserDefaults] objectForKey:VKUserDefaultsLoginTypeKey];
    
    if ( vkLoginType )
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:VKUserDefaultsLoginTypeKey];
    
    vkLoginType = @(newVkLoginType);
    
    [[NSUserDefaults standardUserDefaults] setObject:vkLoginType forKey:VKUserDefaultsLoginTypeKey];
}

@end
