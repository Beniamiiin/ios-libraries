//
//  OYTwitterSocial.m
//
//  Created by Beniamin on 09.06.14.
//

#import "OYTwitterSocial.h"

#import "OYSocialManagerHelper.h"

#import <FHSTwitterEngine.h>

static NSString *const TwitterPlistKey               = @"Twitter";
static NSString *const TwitterPlistAppIDKey          = @"TwitterAppID";
static NSString *const TwitterPlistAppSecretKey      = @"TwitterAppSecret";

static int const kDuplicatePostTwitterError          = 204;

@interface OYTwitterSocial () <FHSTwitterEngineAccessTokenDelegate>
{
    NSString *TwitterAppID;
    NSString *TwitterAppSecret;
}

- (void)p_loginToTwitterWithSuccess:(void (^)())success
                            failure:(void (^)(NSError *error))failure;

- (void)p_postToTwitterWall:(id)message
                    success:(void (^)())success
                    failure:(void (^)(NSError *error))failure;

- (void)p_helperPostToTwitterWall:(id)message
                          success:(void (^)())success
                          failure:(void (^)(NSError *error))failure;

- (void)p_twitterEngineControllerDidCancel;

@end

@implementation OYTwitterSocial

#pragma mark - Public methods -

#pragma mark Initialize
+ (OYTwitterSocial *)setup
{
    return [[self alloc] init];
}

- (id)init
{
    if ( self = [super init] )
    {
        NSDictionary *infoPlistSocialSettings = [[NSBundle mainBundle] objectForInfoDictionaryKey:SocialSettingsPlistKey];
        
        // Setup app ID and SecretKey
        TwitterAppID = [infoPlistSocialSettings[TwitterPlistKey][TwitterPlistAppIDKey] copy];
        TwitterAppSecret = [infoPlistSocialSettings[TwitterPlistKey][TwitterPlistAppSecretKey] copy];
        
        // Init
        [[FHSTwitterEngine sharedEngine] permanentlySetConsumerKey:TwitterAppID andSecret:TwitterAppSecret];
        [[FHSTwitterEngine sharedEngine] setDelegate:self];
        [[FHSTwitterEngine sharedEngine] loadAccessToken];
    }
    
    return self;
}

#pragma mark Authorization methods
- (void)authorizeWithSuccess:(void (^)())success
                     failure:(void (^)(NSError *error))failure
{
    [self p_loginToTwitterWithSuccess:success
                              failure:failure];
}

#pragma mark Post to wall
- (void)postToWall:(id)message
           success:(void (^)())success
           failure:(void (^)(NSError *error))failure
{
    [self p_postToTwitterWall:message
                      success:success
                      failure:failure];
}

#pragma mark Getters
- (BOOL)isSimpleAuthorized
{
    return [[FHSTwitterEngine sharedEngine] isAuthorized];
}

#pragma mark - Private methods -

#pragma mark Authorization methods
- (void)p_loginToTwitterWithSuccess:(void (^)())success
                            failure:(void (^)(NSError *error))failure
{
    NSError *error;
    BOOL internet = [OYSocialManagerHelper hasInternet:&error];
    
    if ( !internet )
    {
        if ( failure && error )
            failure(error);
        
        return;
    }
    
    if ( self.simpleAuthorized )
        return;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(p_twitterEngineControllerDidCancel)
                                                 name:@"FHSTwitterEngineControllerDidCancel"
                                               object:nil];
    
    UIViewController *loginController = [[FHSTwitterEngine sharedEngine] loginControllerWithCompletionHandler:^(BOOL loginSuccess) {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:@"FHSTwitterEngineControllerDidCancel"
                                                      object:nil];
        
        if ( loginSuccess )
        {
            if ( success )
                success();
        }
        else
        {
            NSError *error = [OYSocialManagerHelper createErrorWithTitle:@"Some went wrong"
                                                                 message:@"Twitter login fail"
                                                                    code:OYTwitterLoginError];
            if ( failure )
                failure(error);
        }
    }];
    
    [[OYSocialManagerHelper loginContainer] presentViewController:loginController animated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    }];
}

#pragma mark Post to wall
- (void)p_postToTwitterWall:(id)message
                    success:(void (^)())success
                    failure:(void (^)(NSError *error))failure
{
    NSError *error;
    BOOL internet = [OYSocialManagerHelper hasInternet:&error];
    
    if ( !internet )
    {
        if ( failure && error )
            failure(error);
        
        return;
    }
    
    if ( self.simpleAuthorized )
    {
        [self p_helperPostToTwitterWall:message
                                success:success
                                failure:failure];
        return;
    }
    
    void(^_success)() = ^{
        [self p_postToTwitterWall:message
                          success:success
                          failure:failure];
    };
    
    void(^_failure)(NSError *) = ^(NSError *error)
    {
        if ( failure && error )
            failure(error);
    };
    
    [self authorizeWithSuccess:_success
                       failure:_failure];
}

- (void)p_helperPostToTwitterWall:(id)message
                          success:(void (^)())success
                          failure:(void (^)(NSError *error))failure
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [OYSocialManagerHelper showLoader];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @autoreleasepool {
            id returned;
            
            if ( [message isKindOfClass:[NSDictionary class]] )
            {
                NSString *_message = message[@"message"];
                NSData *imageData = UIImagePNGRepresentation(message[@"picture"]);
                
                returned = [[FHSTwitterEngine sharedEngine] postTweet:_message
                                                        withImageData:imageData];
            }
            else
            {
                returned = [[FHSTwitterEngine sharedEngine] postTweet:message];
            }
            
            if ( [returned isKindOfClass:[NSError class]] )
            {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [OYSocialManagerHelper hideLoader];
                    
                    NSError *error = [self p_handleTwitterPostError:returned];
                    
                    if ( failure )
                        failure(error);
                });
                
                return;
            }
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                [OYSocialManagerHelper hideLoader];
                
                if ( success )
                    success();
            });
        }
    });
}

#pragma mark Helpers
- (NSError *)p_handleTwitterPostError:(NSError *)error
{
    NSString *description = @"Some went wrong";
    NSString *reason = error.localizedDescription;
    NSInteger code = error.code;
    
    if ( error.code == kDuplicatePostTwitterError )
    {
        reason = @"You have already published the following entry on the wall";
        code = OYTwitterPostErrorDuplicatePost;
    }
    
    NSError *_error = [OYSocialManagerHelper createErrorWithTitle:description
                                                          message:reason
                                                             code:code];
    
    return _error;
}

- (void)p_twitterEngineControllerDidCancel
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
}

#pragma mark FHSTwitterEngineAccessTokenDelegate
- (NSString *)loadAccessToken
{
    return [[NSUserDefaults standardUserDefaults]objectForKey:@"SavedAccessHTTPBody"];
}

- (void)storeAccessToken:(NSString *)accessToken
{
    [[NSUserDefaults standardUserDefaults]setObject:accessToken forKey:@"SavedAccessHTTPBody"];
}

@end
