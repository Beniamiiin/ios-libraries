//
//  OYSocial.h
//
//  Created by Beniamin on 09.06.14.
//

#import <Foundation/Foundation.h>

enum
{
    /**
     *  Indicates error if user forbade permissions
     */
    OYNotShowError                      = -1000
};

typedef void(^RequestSuccess)();
typedef void(^RequestFailure)(NSError *error);

extern NSString *const SocialSettingsPlistKey;

@interface OYSocial : NSObject

@property (assign, nonatomic, readonly, getter = isSimpleAuthorized) BOOL simpleAuthorized;
@property (assign, nonatomic, readonly, getter = isAdvancedAuthorized) BOOL advancedAuthorized;

+ (OYSocial *)setup;

- (void)authorizeWithSuccess:(void (^)())success
                     failure:(void (^)(NSError *error))failure;

- (void)postToWall:(id)message
           success:(void(^)())success
           failure:(void(^)(NSError *error))failure;

@end
