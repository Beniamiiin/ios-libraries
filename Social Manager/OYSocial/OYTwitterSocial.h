//
//  OYTwitterSocial.h
//
//  Created by Beniamin on 09.06.14.
//

#import "OYSocial.h"

enum
{
    /**
     *  Indicates twitter login error
     */
    OYTwitterLoginError                         = 1014,
    
    /**
     *  Indicates twitter login error
     */
    OYTwitterPostErrorDuplicatePost             = 1015
};

@interface OYTwitterSocial : OYSocial

@property (assign, nonatomic, readonly, getter = isSimpleAuthorized) BOOL simpleAuthorized;

+ (OYTwitterSocial *)setup;

- (void)authorizeWithSuccess:(void (^)())success
                     failure:(void (^)(NSError *error))failure;

- (void)postToWall:(NSString *)message
           success:(void(^)())success
           failure:(void(^)(NSError *error))failure;

@end
