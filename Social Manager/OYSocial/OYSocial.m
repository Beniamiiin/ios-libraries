//
//  OYSocial.m
//
//  Created by Beniamin on 09.06.14.
//

#import "OYSocial.h"

NSString *const SocialSettingsPlistKey = @"Social Settings";

@implementation OYSocial

#pragma mark Initialize
+ (OYSocial *)setup
{
    return [[self alloc] init];
}

- (id)init
{
    if ( self = [super init] )
    {
        
    }
    
    return self;
}

#pragma mark Authorization methods
- (void)authorizeWithSuccess:(void (^)())success
                     failure:(void (^)(NSError *error))failure
{

}

#pragma mark Post to wall
- (void)postToWall:(id)message
           success:(void(^)())success
           failure:(void(^)(NSError *error))failure
{
    
}

#pragma mark Getters
- (BOOL)isSimpleAuthorized
{
    return NO;
}

- (BOOL)isAdvancedAuthorized
{
    return NO;
}

@end
