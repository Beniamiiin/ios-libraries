//
//  OYVKSocial.h
//
//  Created by Beniamin on 09.06.14.
//

#import "OYSocial.h"

enum
{
    /**
     *  Indicates error if user forbade permissions
     */
    OYVKLoginErrorUserDeniedRequest             = 1010,
    
    /**
     *  Indicates error if user forbade permissions
     */
    OYVKErrorPermissions                        = 1011,
    
    /**
     *  Indicates error if user forbade permissions
     */
    OYVKUnkwonError                             = 1013
};

@interface OYVKSocial : OYSocial

@property (assign, nonatomic, readonly, getter = isSimpleAuthorized) BOOL simpleAuthorized;
@property (assign, nonatomic, readonly, getter = isAdvancedAuthorized) BOOL advancedAuthorized;

+ (OYVKSocial *)setup;

- (void)authorizeWithSuccess:(void (^)())success
                     failure:(void (^)(NSError *error))failure;

- (void)postToWall:(id)message
           success:(void(^)())success
           failure:(void(^)(NSError *error))failure;

@end
