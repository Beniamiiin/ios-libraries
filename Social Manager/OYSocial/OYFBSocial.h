//
//  OYFacebookSocial.h
//
//  Created by Beniamin on 09.06.14.
//

#import "OYSocial.h"

enum
{    
    /**
     *  Indicates unknown error
     */
    OYFacebookLoginErrorUnknown                 = 1000,
    
    /**
     *  Indicates that the error is authentication related and the application should reopen the session
     */
    OYFacebookLoginErrorReopenSessions          = 1001,
    
    /**
     *  Indicates that the error is permission related
     */
    OYFacebookLoginErrorPermissions             = 1002,
    
    /**
     *  Indicates that the error implies that the server had an unexpected failure or may be temporarily down
     */
    OYFacebookLoginErrorServer                  = 1003,
    
    /**
     *  Indicates that the error results from the server throttling the client
     */
    OYFacebookLoginErrorThrottling              = 1004,
    
    /**
     *  Indicates the user cancelled the operation
     */
    OYFacebookLoginErrorUserCancelled           = 1005,
    
    /**
     *  Indicates that the error is Facebook-related but is uncategorizable, and likely newer than the
     *  current version of the SDK
     */
    OYFacebookLoginErrorFacebookOther           = 1006,
    
    /**
     *  Indicates that the error is an application error resulting in a bad or malformed request to the server.
     */
    OYFacebookLoginErrorBadRequest              = 1007,
    
    /**
     *  Indicates known error
     */
    OYFacebookLoginErrorKnown                   = 1008,
    
    /**
     *  Indicates what duplaticate post
     */
    OYFacebookPostErrorDuplicatePost            = 1009
};

@interface OYFBSocial : OYSocial

@property (assign, nonatomic, readonly, getter = isSimpleAuthorized) BOOL simpleAuthorized;
@property (assign, nonatomic, readonly, getter = isAdvancedAuthorized) BOOL advancedAuthorized;

+ (OYFBSocial *)setup;

- (void)authorizeWithSuccess:(void (^)())success
                     failure:(void (^)(NSError *error))failure;

- (void)postToWall:(id)message
           success:(void(^)())success
           failure:(void(^)(NSError *error))failure;

@end
