//
//  OYFacebookSocial.m
//
//  Created by Beniamin on 09.06.14.
//

#import "OYFBSocial.h"

#import "OYSocialManagerHelper.h"

#import <FacebookSDK/FacebookSDK.h>
#import "FBSession+Internal.h"

static NSString *const FBPlistKey                    = @"FB";
static NSString *const FBPlistSimplePermissionsKey   = @"FBSimplePermissions";
static NSString *const FBPlistAdvancedPermissionsKey = @"FBAdvancedPermissions";
static NSString *const FBUserDefaultsLoginTypeKey    = @"FBUserDefaultsLoginTypeKey";

static NSString *const kFBAccessTokenKey             = @"FBAccessTokenKey";

static int const kDuplicatePostFacebookError         = 506;
static int const kPermissionFacebookError            = 200;

typedef NS_ENUM(NSInteger, FBLoginType)
{
    FBLoginTypeUnknown = -1,
    FBLoginWithSimplePermissions,
    FBLoginWithAdvancedPermissions
};

typedef NS_ENUM(NSInteger, FBActionType)
{
    FBActionTypePostToWall,
    FBActionTypeAuthorization
};

@interface OYFBSocial ()
{
    NSArray *FBSimplePermissions;
    NSArray *FBAdvancedPermissions;
}

@property (assign, nonatomic, readwrite) FBLoginType fbLoginType;
@property (assign, nonatomic, readwrite) FBActionType fbActionType;

- (void)p_loginToFacebookWithPermissions:(NSArray *)permissions
                               loginType:(int)loginType
                                 success:(void (^)())success
                                 failure:(void (^)(NSError *error))failure;

- (NSError *)p_handleFBLoginError:(NSError *)error;

- (void)p_postToFacebookWall:(id)message
                     success:(void (^)())success
                     failure:(void (^)(NSError *error))failure;

- (void)p_helperPostToFacebookWall:(id)message
                           success:(void (^)())success
                           failure:(void (^)(NSError *error))failure;

- (void)p_addNewPermissions:(NSArray *)neededPermissions
                    sussess:(void(^)())success
                    failure:(void(^)(NSError *error))failure;

- (void)p_checkNeededPermissionsAlreadyRequested:(NSArray *)neededPermissions
                                         sussess:(void(^)(NSArray *notRequestedPermissions))sussess
                                         failure:(void(^)(NSError *error))failure;

- (void)p_requestNewPermissions:(NSArray *)notRequestedPermissions
                        sussess:(void(^)())success
                        failure:(void(^)(NSError *error))failure;

@end

@implementation OYFBSocial

#pragma mark - Public methods -

#pragma mark Initialize
+ (OYFBSocial *)setup
{
    return [[self alloc] init];
}

- (id)init
{
    if ( self = [super init] )
    {
        NSDictionary *infoPlistSocialSettings = [[NSBundle mainBundle] objectForInfoDictionaryKey:SocialSettingsPlistKey];
        
        // Setup permissions
        FBSimplePermissions = [infoPlistSocialSettings[FBPlistKey][FBPlistSimplePermissionsKey] copy];
        FBAdvancedPermissions = [infoPlistSocialSettings[FBPlistKey][FBPlistAdvancedPermissionsKey] copy];
    }
    
    return self;
}

#pragma mark Authorization methods
- (void)authorizeWithSuccess:(void (^)())success
                     failure:(void (^)(NSError *error))failure
{
    _fbActionType = FBActionTypeAuthorization;
    
    [self p_loginToFacebookWithPermissions:FBAdvancedPermissions
                                 loginType:FBLoginWithAdvancedPermissions
                                   success:success
                                   failure:failure];
}

#pragma mark Post to wall
- (void)postToWall:(id)message
           success:(void (^)())success
           failure:(void (^)(NSError *error))failure
{
    _fbActionType = FBActionTypePostToWall;
    
    [self p_postToFacebookWall:message
                       success:success
                       failure:failure];
}

#pragma mark Getters
- (BOOL)isSimpleAuthorized
{
    return [[FBSession activeSession] isOpen];
}

- (BOOL)isAdvancedAuthorized
{
    return self.fbLoginType == FBLoginWithAdvancedPermissions;
}

#pragma mark - Private methods -

#pragma mark Authorization methods
- (void)p_loginToFacebookWithSimplePermissionsWithSuccess:(void (^)())success
                                                  failure:(void (^)(NSError *error))failure
{
    [self p_loginToFacebookWithPermissions:FBSimplePermissions
                                 loginType:FBLoginWithSimplePermissions
                                   success:success
                                   failure:failure];
}

- (void)p_loginToFacebookWithPermissions:(NSArray *)permissions
                               loginType:(int)loginType
                                 success:(void (^)())success
                                 failure:(void (^)(NSError *error))failure
{
    NSError *error;
    BOOL internet = [OYSocialManagerHelper hasInternet:&error];
    
    if ( !internet )
    {
        if ( failure && error )
            failure(error);
        
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    void (^completionHandler)(FBSession *, FBSessionState, NSError *) = ^(FBSession *session, FBSessionState status, NSError *error)
    {
        if ( error )
        {
            if ( !failure )
                return;
            
            NSError *_error = [weakSelf p_handleFBLoginError:error];
            
            if ( _error )
                failure(_error);
            else if ( error )
                failure(error);
            
            return;
        }
        
        if ( weakSelf.fbLoginType != FBLoginWithAdvancedPermissions )
            weakSelf.fbLoginType = loginType;
        
        if ( success )
            success();
    };
    
    if ( FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded )
    {
        [FBSession.activeSession openWithCompletionHandler:completionHandler];
    }
    else
    {
        [FBSession openActiveSessionWithReadPermissions:permissions
                                           allowLoginUI:YES
                                      completionHandler:completionHandler];
    }
}

- (NSError *)p_handleFBLoginError:(NSError *)error
{
    NSString *reason;
    NSInteger code;
    
    if ( [FBErrorUtility shouldNotifyUserForError:error] )
    {
        reason = [FBErrorUtility userMessageForError:error];
        code = OYFacebookLoginErrorKnown;
    }
    else
    {
        FBErrorCategory category = [FBErrorUtility errorCategoryForError:error];
        
        switch (category)
        {
            case FBErrorCategoryAuthenticationReopenSession:
            {
                reason = @"Current token is not valid, relogin please";
                code = OYFacebookLoginErrorReopenSessions;
            }
                break;
                
            case FBErrorCategoryPermissions:
            {
                reason = @"You don't have permission to perform this action";
                code = OYFacebookLoginErrorPermissions;
            }
                break;
                
            case FBErrorCategoryServer:
            {
                reason = @"Unable to connect to server";
                code = OYFacebookLoginErrorServer;
            }
                break;
                
            case FBErrorCategoryThrottling:
            {
                reason = @"Check facebook settings";
                code = OYFacebookLoginErrorThrottling;
            }
                break;
                
            case FBErrorCategoryUserCancelled:
            {
                reason = @"User cancelled";
                code = OYNotShowError;
            }
                break;
                
            case FBErrorCategoryFacebookOther:
            {
                reason = @"Try again later";
                code = OYFacebookLoginErrorFacebookOther;
                
                NSNumber *errorCode;
                [self p_handleUnknownFacebookError:error
                                            reason:&reason
                                         errorCode:&errorCode];
                
                if ( errorCode )
                    code = errorCode.intValue;
            }
                break;
                
            case FBErrorCategoryBadRequest:
            {
                reason = @"Unable to connect to server";
                code = OYFacebookLoginErrorBadRequest;
            }
                break;
                
            default:
            {
                reason = @"Try again later";
                code = OYFacebookLoginErrorUnknown;
                
                NSNumber *errorCode;
                [self p_handleUnknownFacebookError:error
                                            reason:&reason
                                         errorCode:&errorCode];
                
                if ( errorCode )
                    code = errorCode.intValue;
            }
                break;
        }
    }
    
    NSError *_error = [OYSocialManagerHelper createErrorWithTitle:@"Something went wrong"
                                                          message:reason
                                                             code:code];
    
    return _error;
}

- (void)p_handleUnknownFacebookError:(NSError *)error reason:(NSString **)reason errorCode:(NSNumber **)code
{
    int errorCode = [error.userInfo[@"com.facebook.sdk:ParsedJSONResponseKey"][@"body"][@"error"][@"code"] intValue];
    
    if ( errorCode == kDuplicatePostFacebookError )
    {
        *reason = @"You have already published the following entry on the wall";
        *code = @(OYFacebookPostErrorDuplicatePost);
    }
    
    if ( errorCode >= kPermissionFacebookError && errorCode < kPermissionFacebookError+100 )
    {
        *reason = @"You don't have permission to perform this action";
        *code = @(OYFacebookLoginErrorPermissions);
    }
}

#pragma mark Post to wall
- (void)p_postToFacebookWall:(id)message
                     success:(void (^)())success
                     failure:(void (^)(NSError *error))failure
{
    NSError *error;
    BOOL internet = [OYSocialManagerHelper hasInternet:&error];
    
    if ( !internet )
    {
        if ( failure && error )
            failure(error);
        
        return;
    }
    
    if ( self.isSimpleAuthorized )
    {
        [self p_helperPostToFacebookWall:message
                                 success:success
                                 failure:failure];
        return;
    }

    __weak typeof(self) weakSelf = self;
    
    void(^_success)() = ^{
        [weakSelf p_helperPostToFacebookWall:message
                                     success:success
                                     failure:failure];
    };
    
    void(^_failure)(NSError *) = ^(NSError *error)
    {
        if ( failure && error )
            failure(error);
    };
    
    [self p_loginToFacebookWithSimplePermissionsWithSuccess:_success
                                                    failure:_failure];
}

#pragma mark Update facebook permissions
- (void)p_updateFacebookPermissionsToAdvancedWithSuccess:(void (^)())success
                                                 failure:(void (^)(NSError *error))failure
{
    void(^_success)() = ^{
        self.fbLoginType = FBLoginWithAdvancedPermissions;
        
        if ( success )
        {
            success();
        }
    };
    
    void(^_failure)(NSError *) = ^(NSError *error)
    {
        if ( !failure )
            return;
        
        NSError *_error = [self p_handleFBLoginError:error];
        
        if ( _error )
            failure(_error);
        else if ( error )
            failure(error);
    };
    
    [self p_addNewPermissions:FBAdvancedPermissions
                      sussess:_success
                      failure:_failure];
}

#pragma mark Helpers
- (void)p_helperPostToFacebookWall:(id)message
                           success:(void (^)())success
                           failure:(void (^)(NSError *error))failure
{
    if ( _fbActionType != FBActionTypePostToWall )
        return;
    
    [OYSocialManagerHelper showLoader];

    void(^completion)(FBRequestConnection*, id, NSError*) = ^(FBRequestConnection *connection, id result, NSError *error)
    {
        [OYSocialManagerHelper hideLoader];
        
        if ( error )
        {
            NSError *_error = [self p_handleFBLoginError:error];
            
            if ( failure )
                failure(_error);
            
            return;
        }
        
        if ( success )
            success();
    };
    
    if ( [message isKindOfClass:[NSDictionary class]] )
    {
        [FBRequestConnection startWithGraphPath:@"me/feed"
                                     parameters:message
                                     HTTPMethod:@"POST"
                              completionHandler:completion];
    }
    else
    {
        [FBRequestConnection startForPostStatusUpdate:message
                                    completionHandler:completion];
    }
}

- (void)p_addNewPermissions:(NSArray *)neededPermissions
                    sussess:(void(^)())success
                    failure:(void(^)(NSError *error))failure
{
    [self p_checkNeededPermissionsAlreadyRequested:neededPermissions
                                           sussess:^(NSArray *notRequestedPermissions) {
                                               if ( !notRequestedPermissions.count )
                                               {
                                                   if ( success )
                                                       success();
                                                   
                                                   return;
                                               }
                                               
                                               [self p_requestNewPermissions:notRequestedPermissions
                                                                     sussess:success
                                                                     failure:failure];
                                           } failure:^(NSError *error) {
                                               if ( failure )
                                                   failure(error);
                                           }];
}

- (void)p_checkNeededPermissionsAlreadyRequested:(NSArray *)neededPermissions
                                         sussess:(void(^)(NSArray *notRequestedPermissions))sussess
                                         failure:(void(^)(NSError *error))failure
{
    [FBRequestConnection startWithGraphPath:@"/me/permissions"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                              if ( error )
                              {
                                  if ( failure )
                                      failure(error);
                                  
                                  return;
                              }
                              
                              NSArray *currentPermissionsArray = (NSArray *)[result data];
                              NSMutableArray *requestPermissions = [@[] mutableCopy];
                              NSMutableArray *currentPermissionsMutableArray = [@[] mutableCopy];
                              
                              for (NSDictionary *currentPermission in currentPermissionsArray)
                              {
                                  [currentPermissionsMutableArray addObject:currentPermission[@"permission"]];
                              }
                              
                              for (NSString *permission in neededPermissions)
                              {
                                  if ( ![currentPermissionsMutableArray containsObject:permission] )
                                      [requestPermissions addObject:permission];
                              }
                              
                              if ( sussess )
                                  sussess(requestPermissions);
                          }];
}

- (void)p_requestNewPermissions:(NSArray *)notRequestedPermissions
                        sussess:(void(^)())success
                        failure:(void(^)(NSError *error))failure
{
    [FBSession.activeSession requestNewPublishPermissions:notRequestedPermissions
                                          defaultAudience:FBSessionDefaultAudienceEveryone
                                        completionHandler:^(FBSession *session, NSError *error) {
                                            if ( error )
                                            {
                                                if ( failure )
                                                    failure(error);
                                                
                                                return;
                                            }
                                            
                                            if ( success )
                                                success();
                                        }];
}

#pragma mark User defaults
- (FBLoginType)fbLoginType
{
    NSNumber *fbLoginType = [[NSUserDefaults standardUserDefaults] objectForKey:FBUserDefaultsLoginTypeKey];
    
    if ( !fbLoginType )
        return FBLoginTypeUnknown;
    
    return fbLoginType.longValue;
}

- (void)setFbLoginType:(FBLoginType)newFbLoginType
{
    NSNumber *fbLoginType = [[NSUserDefaults standardUserDefaults] objectForKey:FBUserDefaultsLoginTypeKey];
    
    if ( fbLoginType )
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:FBUserDefaultsLoginTypeKey];
    
    fbLoginType = @(newFbLoginType);
    
    [[NSUserDefaults standardUserDefaults] setObject:fbLoginType forKey:FBUserDefaultsLoginTypeKey];
}

@end
