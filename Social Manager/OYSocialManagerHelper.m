//
//  OYSocialManagerHelper.m
//
//  Created by Beniamin on 09.06.14.
//

#import "OYSocialManagerHelper.h"

#import "OYNetReachability.h"

#import <MBProgressHUD.h>
#import <OYLoader.h>

NSString *const OYSocialErrorDomain = @"BSSocialErrorDomain";

static UIViewController *loaderContainer;
static OYLoaderType loaderType;

@implementation OYSocialManagerHelper

#pragma mark - Public methods -
+ (void)showLoader
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    if ( loaderType == OYLoaderTypeDefault )
        [MBProgressHUD showHUDAddedTo:[OYSocialManagerHelper loginContainer].view animated:YES];
    else
        [OYLoader showLoaderAddedTo:[OYSocialManagerHelper loginContainer].view];
}

+ (void)hideLoader
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if ( loaderType == OYLoaderTypeDefault )
        [MBProgressHUD hideHUDForView:[OYSocialManagerHelper loginContainer].view animated:YES];
    else
        [OYLoader hideLoaderForView:[OYSocialManagerHelper loginContainer].view];
}

+ (NSError *)createErrorWithTitle:(NSString *)title
                          message:(NSString *)message
                             code:(NSInteger)code
{
    NSString *description = title;
    NSString *reason = message;
    NSArray *objArray = @[NSLocalizedString(description, @""), NSLocalizedString(reason, @"")];
    NSArray *keyArray = @[NSLocalizedDescriptionKey, NSLocalizedFailureReasonErrorKey];
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjects:objArray forKeys:keyArray];
    NSError *error = [NSError errorWithDomain:OYSocialErrorDomain code:code userInfo:userInfo];
    
    return error;
}

+ (BOOL)hasInternet:(NSError **)error
{
    if ( ![OYNetReachability sharedInstance].hasInternetConnection )
    {
        if ( error )
        {
            *error = [self createErrorWithTitle:@"Attention"
                                        message:@"Problem with internet connection"
                                           code:OYLostInternetConnection];
        }
        
        return NO;
    }
    
    return YES;
}

+ (void)setLoaderContainer:(UIViewController *)container
{
    loaderContainer = container;
}

+ (void)setLoaderType:(OYLoaderType)type
{
    loaderType = type;
}

#pragma mark - Private methods -
+ (UIViewController *)loginContainer
{
    if ( loaderContainer )
        return loaderContainer;
    
    static UIWindow *window;
    
    if ( window )
        return window.rootViewController;
    
    window = [UIApplication sharedApplication].keyWindow;
    
    if ( window.windowLevel != UIWindowLevelNormal )
    {
        for (window in [UIApplication sharedApplication].windows)
        {
            if ( window.windowLevel == UIWindowLevelNormal )
                break;
        }
    }
    
    return window.rootViewController;
}

@end
